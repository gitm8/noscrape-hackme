const express = require('express')
const { join } = require('path');

const hackme01 = express()
const maxFields = 10;

hackme01.set('views', join(__dirname, 'views'));

hackme01.get('/', (req, res, next) => {
  res.render('login', {
    stackHeight: Math.floor(Math.random() * 10),
    fieldNumber: Math.floor(Math.random() * (maxFields + 1)),
    maxFields,
  })
})

module.exports = hackme01;
