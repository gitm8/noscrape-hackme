const express = require('express');
const hackmes = require('./hackmes')

const server = express();

server.set('view engine', 'ejs');
server.set('views', 'src/views');

server.get('/', (req, res, next) => {
  res.render('index', { hackmes: hackmes.map(({ urlPath}) => urlPath) })
})

hackmes.forEach(({ urlPath, router }) => {
  server.use(`/noscrape/${urlPath}`, router)
})

server.listen(8080, () => {
  console.log('NoScrape HackMe server listening @ http://localhost:8080')
})