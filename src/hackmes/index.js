const { lstatSync, readdirSync } = require('fs')
const { join } = require('path')

const isDirectory = source => lstatSync(source).isDirectory()

function readDirectories(source) {
  const list = readdirSync(source);
  const filteredList = list.filter(dirName => isDirectory(join(source, dirName)));

  return filteredList.map(dirName => ({
    urlPath: dirName,
    router: require(join(source, dirName))
  }))
}

module.exports = readDirectories(__dirname);
